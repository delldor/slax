#TODO: manejo de decimal. como se corta, subir uno si es mayor a .4
LANG=C
bate=$(find /sys/class/power_supply/ -iname "BAT*"|tail -n 1)

valor(){
echo "$(cat $bate/charge_now),$(cat $bate/charge_full)" | awk -F, '{ ress=$1/$2 *100} END {  print  ress }' | cut -d. -f1
}

estado(){
cat $(echo $bate/status)
}

while true 
do
porce=$(valor)
stado=$(estado)
[ $porce -le 42 ] && [ "$stado" = "Discharging" ] && \
xmessage -center -timeout 15 "Bateria baja. Conecte. $porce% "

[ $porce -ge 80 ] && [ "$stado" =  "Charging" ] && \
xmessage -center -timeout 15 "Bateria alta. Desconecte. $porce% "
sleep 60
done