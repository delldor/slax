#[Desde](https://github.com/Tomas-M/linux-live/blob/master/Slax/debian/cleanup) 
#y para [chromium](https://github.com/Tomas-M/linux-live/blob/master/Slax/debian/modules/05-chromium/build)
#[modulos](https://github.com/Tomas-M/linux-live/tree/master/Slax/debian/modules)

rm -Rf /usr/share/icons/gnome/256x256
rm -Rf /usr/share/applications/chromium.desktop
rm -r /usr/share/icons/Tango

rm -Rf /usr/share/locale/??
rm -Rf /usr/share/locale/??_*
rm -Rf /usr/share/locale/??@*
rm -Rf /usr/share/locale/???
rm -Rf /usr/share/i18n/locales/*_*
rm -Rf /usr/share/man/??
rm -Rf /usr/share/man/*_*
rm -f /var/lib/dpkg/*-old
rm -f /var/log/*
rm -f /var/log/*/*
rm -f /var/log/*/*/*
rm -f /var/cache/debconf/*-old

rm -vr ./home/guest/.cache/
rm -rv ./var/log/
rm -v ./root/.thumbnails/{normal,large}/*