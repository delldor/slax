#https://stackoverflow.com/a/35927479
#Opciones: https://www.worldtimeserver.com/hora-exacta-VE.aspx https://time.is/es/Venezuela
#Verifica root
#sudo -s <<'EOF'
if [ "$UID" != "0" -a "$UID" != "" ]; then
   echo "You are not root. You must run as root."
   exit 1
else
[ -e /root/.cache/delldor/fecha ] && $(date --set="$(cat /root/.cache/delldor/fecha)"
) || $(date -s "06/28/2018 12:00:00")

#until ping -nq -c3 8.8.8.8; do #verifica conexión 
#sleep 3s
#done
[ "$(ping -nq -c3 8.8.8.8)" ] && echo "Hay conexión" || reconecta 
Fecha=$(curl -IL https://www.timeanddate.com/worldclock |grep date | tail -n1| cut -d':' -f2-)
fecha2=$(date +"%Y-%m-%d %H:%M:%S" -d "$Fecha")
ln -fs /usr/share/zoneinfo/America/Caracas /etc/localtime
date -s "$fecha2"
mkdir -p /root/.cache/delldor
#FIXME: parece que a cuertas horas, set da error al leerla
date +"%Y%m%d %H:%M:%S" > /root/.cache/delldor/fecha
echo "Ok"
fi
#EOF
