rm /root/.asoundrc
#Tarjeta
tarj=$(aplay -l | awk -F: '/card/ { print $1 }'| awk '{ print $2 }'|tail -n1)
#Dispositivo
dispo=$(aplay -l | awk -F: '/card/ { print $2 }' | cut -d, -f2| awk '{print $2 }'|tail -n1)

echo "defaults.pcm.card $tarj
defaults.pcm.device $dispo
defaults.ctl.card $tarj" | tee /root/.asoundrc
rm /home/guest/.asoundrc
ln -s /root/.asoundrc /home/guest/
aplay /usr/share/sounds/startup.wav
